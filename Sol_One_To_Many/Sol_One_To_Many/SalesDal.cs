﻿using Sol_One_To_Many.EF;
using Sol_One_To_Many.Entity;
using Sol_One_To_Many.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_One_To_Many
{
    public class SalesDal
    {
        #region  declaration

        private AdventureWorks2012Entities1 db = null;

        #endregion

        #region  constructor

        public SalesDal()
        {
            db = new AdventureWorks2012Entities1();
        }

        #endregion

        #region  public methods

        public async Task<IEnumerable<SalesHeaderEntity>> GetSalesHeaderData()
        {
            try
            {
                return await Task.Run(() =>
                {
                    var getQuery =
                    db
                    ?.SalesOrderHeaders
                    ?.AsEnumerable()
                    ?.Select((leSalesOrderHeaderObj) => new SalesHeaderEntity()
                    {
                        SalesOrderId = leSalesOrderHeaderObj.SalesOrderID,
                        SalesOrderNo = leSalesOrderHeaderObj?.SalesOrderNumber,
                        salesOrderDetailEntityObj = this.GetSalesDetailData(leSalesOrderHeaderObj).Result

                    })?.ToList();


                    return getQuery;

                    #region  //gettin error in dis segment of code when using interface...
                    //var getQuery =
                    //db
                    //?.SalesOrderHeaders
                    //?.AsEnumerable()
                    //?.Select((leSalesOrderHeaderObj) => new SalesHeaderEntity()
                    //{
                    //    SalesOrderId = leSalesOrderHeaderObj.SalesOrderID,
                    //    SalesOrderNo = leSalesOrderHeaderObj?.SalesOrderNumber,
                    //    salesOrderDetailEntityObj = leSalesOrderHeaderObj?.SalesOrderDetails
                    //                                ?.AsEnumerable()
                    //                                ?.Select((leSalesOrderDetailObj) => new SalesDetailEntity()
                    //                                {
                    //                                    SalesOrderId = leSalesOrderDetailObj.SalesOrderID,
                    //                                    OrderQty = leSalesOrderDetailObj.OrderQty,
                    //                                    UnitPrice = leSalesOrderDetailObj.UnitPrice
                    //                                })
                    //                                ?.ToList()

                    //})
                    //    ?.ToList();

                    //return getQuery;

                    #endregion

                });
            }
            catch (Exception)
            {

                throw;
            }
           

        }

        #endregion

        #region  private methods

        private async Task<List<SalesDetailEntity>> GetSalesDetailData(SalesOrderHeader salesOrderHeaderObj)
        {
            try
            {
                return await Task.Run(() =>
                {
                    var getQuery1 =
                    salesOrderHeaderObj
                    ?.SalesOrderDetails
                    ?.AsEnumerable()
                    ?.Select((leSalesOrderDetailObj) => new SalesDetailEntity()
                    {
                        SalesOrderId = leSalesOrderDetailObj.SalesOrderID,
                        OrderQty = leSalesOrderDetailObj.OrderQty,
                        UnitPrice = leSalesOrderDetailObj.UnitPrice
                    })
                        ?.ToList();

                    return getQuery1;
                });
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        #endregion
    }
}
