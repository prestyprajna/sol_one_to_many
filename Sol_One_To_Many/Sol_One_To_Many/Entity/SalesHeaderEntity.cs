﻿using Sol_One_To_Many.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_One_To_Many.Entity
{
    public class SalesHeaderEntity: ISalesHeaderEntity
    {
        public int SalesOrderId { get; set; }

        public string SalesOrderNo { get; set; }

        public List<SalesDetailEntity> salesOrderDetailEntityObj { get; set; }
    }
}
