﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_One_To_Many.Entity.Interface
{
    public interface ISalesHeaderEntity
    {
        int SalesOrderId { get; set; }

        string SalesOrderNo { get; set; }

        List<SalesDetailEntity> salesOrderDetailEntityObj { get; set; }
    }
}
