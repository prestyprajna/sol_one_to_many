﻿using Sol_One_To_Many.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_One_To_Many
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async() =>
            {
                try
                {
                    IEnumerable<SalesHeaderEntity> salesHeaderEntityObj = await new SalesDal().GetSalesHeaderData();
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
                
            }).Wait();
        }
    }
}
